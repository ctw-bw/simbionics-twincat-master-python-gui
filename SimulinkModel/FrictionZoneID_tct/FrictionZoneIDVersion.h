﻿// ****************** FrictionZoneIDVersion.h *******************************
// Generated by TwinCAT Target for MATLAB/Simulink (TE1400)
// MATLAB R2018b (win64)
// TwinCAT 3.1.4024
// TwinCAT Target 1.2.1240
// Beckhoff Automation GmbH & Co. KG     (www.beckhoff.com)
// *************************************************************
#ifndef FRICTIONZONEIDVERSION_H_INCLUDED
#define FRICTIONZONEIDVERSION_H_INCLUDED

////////////////////////////////////////////////////////////////
//  Version Information
////////////////////////////////////////////////////////////////

#define TWINCAT_TARGET_VERSION  1.2.1240
#define MATLAB_RELEASE	"R2018b"

#endif
