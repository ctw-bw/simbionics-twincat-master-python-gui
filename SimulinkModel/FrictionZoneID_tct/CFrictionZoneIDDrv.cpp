﻿// ****************** CFrictionZoneIDDrv.cpp *******************************
// Generated by TwinCAT Target for MATLAB/Simulink (TE1400)
// MATLAB R2018b (win64)
// TwinCAT 3.1.4024
// TwinCAT Target 1.2.1240
// Beckhoff Automation GmbH & Co. KG     (www.beckhoff.com)
// *************************************************************
#include "TcPch.h"
#pragma hdrstop
#define DEVICE_MAIN

#include "CFrictionZoneIDDrv.h"
#include "CFrictionZoneID.h"
#include "CFrictionZoneIDClassFactory.h"
#include "FrictionZoneIDVersion.h"

DECLARE_GENERIC_DEVICE(FrictionZoneIDDRV)
#undef DEVICE_MAIN

IOSTATUS CFrictionZoneIDDrv::OnLoad( )
{
	TRACE(_T("CFrictionZoneIDClassFactory::OnLoad()\n") );
	m_pObjClassFactory = new CFrictionZoneIDClassFactory();

	return IOSTATUS_SUCCESS;
}

VOID CFrictionZoneIDDrv::OnUnLoad( )
{
	delete m_pObjClassFactory;
}

unsigned long _cdecl CFrictionZoneIDDrv::FrictionZoneID_GetVersion( )
{
	return(0);
}