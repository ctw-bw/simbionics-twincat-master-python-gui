IDsignal = [];
SamplingPeriod = 0.01;

%% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

%% Friction ID

% 0.1A steps from 0 to 1 Amp
Duration = 5; %seconds
Amplitude = 2;
frequency = 0.2;
for idx=1:9
    Amplitude = 0.1*idx;
    newSignal = Amplitude * ones(1,Duration/SamplingPeriod);
    IDsignal = [IDsignal newSignal];
end

% 0.2A steps from 1 to 4 Amp
Duration = 5; %seconds
Amplitude = 2;
frequency = 0.2;
for idx=10:2:40
    Amplitude = 0.1*idx;
    newSignal = Amplitude * ones(1,Duration/SamplingPeriod);
    IDsignal = [IDsignal newSignal];
end

% Repeats signal once again
IDsignal = [IDsignal IDsignal];

% Repeats signal once again but negative
IDsignal = [IDsignal -IDsignal];

%% Zero Band at the end
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

%% Calculates important info for simulation
vectorLength = length(IDsignal);