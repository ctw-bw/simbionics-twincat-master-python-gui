﻿// ****************** SimpleStepSign_types.h *******************************
// Generated by TwinCAT Target for MATLAB/Simulink (TE1400)
// MATLAB R2018b (win64)
// TwinCAT 3.1.4024
// TwinCAT Target 1.2.1240
// Beckhoff Automation GmbH & Co. KG     (www.beckhoff.com)
// *************************************************************
/*
 * SimpleStepSign_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "SimpleStepSign".
 *
 * Model version              : 1.19
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C++ source code generated on : Thu Oct 28 14:40:29 2021
 *
 * Target selection: TwinCAT.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_SimpleStepSign_types_h_
#define RTW_HEADER_SimpleStepSign_types_h_

/* Forward declaration for rtModel */
typedef struct tag_RTM_SimpleStepSign_T RT_MODEL_SimpleStepSign_T;

#endif                                 /* RTW_HEADER_SimpleStepSign_types_h_ */
