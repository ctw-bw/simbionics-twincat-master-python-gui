IDsignal = [];
SamplingPeriod = 0.01;

%% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

%% Zero crossing Identification
% Triangle wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 0.2;
newSignal = Amplitude * sawtooth(pi/2 + 2*pi*frequency*(0:SamplingPeriod:Duration), 0.5);
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 0.2;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 0.296;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 0.438;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 0.648;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 0.960;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 1.420;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 2.102;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 3.111;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 4.604;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 6.814;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 2;
frequency = 10.084;
newSignal = Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

%% Offset Positive Identification

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2;
frequency = 0.2;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 0.296;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 0.438;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 0.648;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 0.960;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 1.420;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 2.102;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 3.111;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 4.604;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 6.814;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = 2; 
frequency = 10.084;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

%% Offset Negative Identification

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 0.2;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 0.296;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 0.438;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 0.648;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 0.960;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 1.420;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 2.102;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 3.111;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 4.604;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 6.814;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];

% Sine Wave
Duration = 30; %seconds
Amplitude = 0.5;
offset = -2;
frequency = 10.084;
newSignal = offset + Amplitude * sin(2*pi*frequency*(0:SamplingPeriod:Duration));
IDsignal = [IDsignal newSignal];

% Zero Band
Duration = 5; %seconds
newSignal = zeros(1,Duration/SamplingPeriod);
IDsignal = [IDsignal newSignal];


%% Calculates important info for simulation
vectorLength = length(IDsignal);