﻿// ****************** StepSignServices.h *******************************
// Generated by TwinCAT Target for MATLAB/Simulink (TE1400)
// MATLAB R2018b (win64)
// TwinCAT 3.1.4024
// TwinCAT Target 1.2.1240
// Beckhoff Automation GmbH & Co. KG     (www.beckhoff.com)
// *************************************************************
#ifndef STEPSIGNSERVICES_H_INCLUDED
#define STEPSIGNSERVICES_H_INCLUDED

#define SRVNAME_StepSign "StepSign"

// TcCOM-ClassId of this module: 27a862bd-06cb-4d85-a064-d365f9c4ffdc
const   CTCID   CID_StepSign    =  {0x27a862bd,0x06cb,0x4d85,{0xa0,0x64,0xd3,0x65,0xf9,0xc4,0xff,0xdc}};

#endif


