﻿// ****************** CStepSign2016bDrv.cpp *******************************
// Generated by TwinCAT Target for MATLAB/Simulink (TE1400)
// MATLAB R2016b (win64)
// TwinCAT 3.1.4024
// TwinCAT Target 1.2.1240
// Beckhoff Automation GmbH & Co. KG     (www.beckhoff.com)
// *************************************************************
#include "TcPch.h"
#pragma hdrstop
#define DEVICE_MAIN

#include "CStepSign2016bDrv.h"
#include "CStepSign2016b.h"
#include "CStepSign2016bClassFactory.h"
#include "StepSign2016bVersion.h"

DECLARE_GENERIC_DEVICE(StepSign2016bDRV)
#undef DEVICE_MAIN

IOSTATUS CStepSign2016bDrv::OnLoad( )
{
	TRACE(_T("CStepSign2016bClassFactory::OnLoad()\n") );
	m_pObjClassFactory = new CStepSign2016bClassFactory();

	return IOSTATUS_SUCCESS;
}

VOID CStepSign2016bDrv::OnUnLoad( )
{
	delete m_pObjClassFactory;
}

unsigned long _cdecl CStepSign2016bDrv::StepSign2016b_GetVersion( )
{
	return(0);
}