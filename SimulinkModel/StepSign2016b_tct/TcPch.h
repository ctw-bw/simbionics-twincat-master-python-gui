﻿// ****************** TcPch.h *******************************
// Generated by TwinCAT Target for MATLAB/Simulink (TE1400)
// MATLAB R2016b (win64)
// TwinCAT 3.1.4024
// TwinCAT Target 1.2.1240
// Beckhoff Automation GmbH & Co. KG     (www.beckhoff.com)
// *************************************************************
///////////////////////////////////////////////////////////////////////////////
// TcPch.h includes TwinCAT standard header files, 
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#include "TcDef.h"
#include "math.h"

#if defined(TC_ENVIROMENT_UM) || defined(TC_ENVIROMENT_CE)
#include <atlbase.h>
extern CComModule _Module;
#include <atlcom.h>
#endif // defined(TC_ENVIROMENT_UM) || defined(TC_ENVIROMENT_CE)

#include "TcBase.h"
#include "TcError.h"
#include "OsBase.h"
