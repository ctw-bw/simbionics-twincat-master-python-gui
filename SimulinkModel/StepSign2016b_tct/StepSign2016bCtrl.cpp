﻿// ****************** StepSign2016bCtrl.cpp *******************************
// Generated by TwinCAT Target for MATLAB/Simulink (TE1400)
// MATLAB R2016b (win64)
// TwinCAT 3.1.4024
// TwinCAT Target 1.2.1240
// Beckhoff Automation GmbH & Co. KG     (www.beckhoff.com)
// *************************************************************
#include "TcPch.h"
#pragma hdrstop

#include "StepSign2016bW32.h"
#include "StepSign2016bCtrl.h"